import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;

public class Employe {
    private int num;
    private String nom;
    private String prenom;
    private String telephone;
    private int service;


    public Employe(int num, String nom, String prenom, String telephone, int service) {
        this.num = num;
        this.nom = nom;
        this.prenom = prenom;
        this.service = service;
        this.telephone=telephone;
    }


    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }
    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }
    public int service() {
        return service;
    }

    public void service(int service) {
        this.service = service;
    }

}
