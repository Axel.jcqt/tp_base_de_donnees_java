CREATE DATABASE IF NOT EXISTS `RH` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `RH`;

CREATE TABLE `SERVICE` (
  `nums` int,
  `noms` varchar(30),
  PRIMARY KEY (`nums`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `EMPLOYE` (
  `nume` int,
  `nome` varchar(30),
  `prenome` varchar(30),
  `telephone` decimal(10),
  `nums` int,
  PRIMARY KEY (`nume`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `CONTRAT` (
  `numc` int,
  `datedebut` date,
  `datefin` date,
  `salaire` decimal(8,2),
  `nume` int,
  PRIMARY KEY (`numc`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `EMPLOYE` ADD FOREIGN KEY (`nums`) REFERENCES `SERVICE` (`nums`);
ALTER TABLE `CONTRAT` ADD FOREIGN KEY (`nume`) REFERENCES `EMPLOYE` (`nume`);

REVOKE ALL privileges ON rh.* FROM adminrh; -- Enlève permition
drop user if exists adminrh; -- supprime l'utilisateur
create user if not exists adminrh identified by '123';

