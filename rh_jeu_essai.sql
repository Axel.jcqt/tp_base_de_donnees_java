insert into SERVICE(nums,noms) values (1,'Direction'),(2,'Finances'),(3,'Informatique'),(4,'Commercial');
insert into EMPLOYE (nume,nome,prenome,telephone,nums) values
            (1,'Theboss','Gilberte','0238000001',1),
            (2,'Menvussa','Gérard','0238000002',1),
            (3,'Zeblouse','Agathe','0238000003',1),
            (4,'Picsous','Arnold','0238000015',2),
            (5,'Zionficale','Eva', '0238000012',2),
            (6,'Tenette','Alain','0238000022',3),
            (7,'Monmodepace','Philémon','0238000005',3),
            (8,'Pasdebug','Laura','0238000022',3),
            (9,'Honime','Anne','0238000012',2),
            (10,'Ouvel','Helene','0238000067',3);

insert into CONTRAT(numc,datedebut,datefin,salaire,nume) values
            (101,'2016-05-22',NULL,50000.12,1),
            (102,'2016-05-23','2018-05-22',1500,2),
            (103,'2018-05-23','2025-05-22',2200,2),
            (104,'2019-06-14',NULL,35000,3),
            (105,'2018-08-25',NULL,24000,4),
            (106,'2018-08-25',NULL,23000,5),
            (107,'2018-08-25','2028-08-24',21000,6),
            (108,'2018-08-25',NULL,31000,7),
            (109,'2018-10-01','2020-09-30',22000,8),
            (110,'2020-10-01',NULL,30999,8),
            (111,'2018-10-01','2020-09-30',18000,9),
            (112,'2022-06-01','2023-06-01',17000,10);

